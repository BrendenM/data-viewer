package com.nexteraenergy.velocity.dataviewer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.orm.jpa.EntityScan;

@SpringBootApplication(scanBasePackages = { "com.nexteraenergy.velocity.dataviewer"},
        scanBasePackageClasses = { DummyService.class, DummyController.class})
public class DataviewerApp {
    public static void main(String[] args) {
        SpringApplication.run(DataviewerApp.class, args);
    }
}
