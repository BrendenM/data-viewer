/**
 * Created by bmorales on 5/11/17.
 */
export interface Series {
  name: string;
  site: string;
  color: string;
  data: any;
}
