import {Action} from "@ngrx/store";
import {Graph} from "./graph.model";
import {Series} from "./series.model";

export class GraphsActions {

  static ADD_GRAPH = '[Graphs] Add Graph';
  addGrpah(graph: Graph): Action {
    return {
      type: GraphsActions.ADD_GRAPH,
      payload: graph
    };
  }

  static SELECT_GRAPH = '[Graphs] Select Graph';
  selectGraph(id: string): Action {
    return {
      type: GraphsActions.SELECT_GRAPH,
      payload: id
    };
  }

  static EDIT_GRAPH = '[Graphs] Edit Graph';
  editGraph(graph: Graph): Action {
    return {
      type: GraphsActions.EDIT_GRAPH,
      payload: graph
    };
  }

  static ADD_SERIES = '[Graphs] Add Series';
  addSeries(graph: Graph, series: Series): Action {
    return {
      type: GraphsActions.EDIT_GRAPH,
      payload: {
        graph: graph,
        series: series
      }
    };
  }

  static CHANGE_DATES = '[Graphs] Change Dates';
  changeDates(graph: Graph): Action {
    return {
      type: GraphsActions.EDIT_GRAPH,
      payload: graph
    };
  }

  static SERIES_DATA = '[Graphs] Series Data';


}
