import {Graph} from "./graph.model";
import {Action, State} from "@ngrx/store";
import {GraphsActions} from "./graphs.actions";

export interface GraphsState {
  graphsMap: { [id: string]: Graph };
  ids: string[];
  selectedId: string;
  nextId: number;
  updating: boolean;
}

export const initialState: GraphsState = {
  graphsMap: {},
  ids: [],
  selectedId: null,
  nextId: 0,
  updating: false
};

export function graphsReducer(state = initialState, action: Action): GraphsState {
  switch (action.type) {

    case GraphsActions.ADD_GRAPH: {

      let newMap = Object.assign({}, state.graphsMap);
      let id = "Graph_" + state.nextId.toString();

      newMap["Graph_" + state.nextId] = {
        name : "Graph " + (state.nextId + 1),
        startTime: new Date(Date.now() - 604800000),
        endTime: new Date(Date.now()),
        id: id,
        series: {}
      };

      let newIds = [... state.ids, id];
      let newNextId = state.nextId + 1;

      let newState = {
        graphsMap: newMap,
        ids: newIds,
        selectedId: id,
        nextId: newNextId,
        updating: false,
        series: {}
      };

      return newState;
    }

    case GraphsActions.SELECT_GRAPH: {
      let newState = Object.assign({}, state );
      newState.selectedId = action.payload;
      return newState;
    }

    case GraphsActions.EDIT_GRAPH: {
      let newState = Object.assign({}, state);
      newState.graphsMap[action.payload.id] = action.payload;
      return newState;
    }

    case GraphsActions.ADD_SERIES: {
      let newState = Object.assign({}, state);
      newState.graphsMap[action.payload.graph.id] = action.payload.graph;
      newState.graphsMap[action.payload.graph.id].series[action.payload.series.name] = action.payload.series;
      return newState;
    }

    case GraphsActions.SERIES_DATA: {
      console.log("SERIES DATA");
      let newState = Object.assign({}, state);
      let newGraph = {
        name: action.payload.graph.name,
        startTime: action.payload.graph.startTime,
        endTime: action.payload.graph.endTime,
        id: action.payload.graph.id,
        series: action.payload.graph.series
      };
      newGraph.series[action.payload.series.name].data = action.payload.data;
      newState.graphsMap[action.payload.graph.id] = newGraph;
      return newState;
    }

    case GraphsActions.CHANGE_DATES: {
      let newState = Object.assign({}, state);
      newState.graphsMap[action.payload.id] = action.payload;
      return newState;
    }


    default: {
      return state;
    }
  }
}

export const getGraphsMap = (state: GraphsState) => state.graphsMap;
export const getIds = (state: GraphsState) => state.ids;
export const getAll = (state: GraphsState) => {
  return state.ids.map( id => state.graphsMap[id]);
};
export const getSelected = (state: GraphsState) => {
  return state.graphsMap[state.selectedId];
};
