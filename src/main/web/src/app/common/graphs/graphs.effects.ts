import {Injectable} from "@angular/core";
import {Actions, Effect} from "@ngrx/effects";
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {GraphsActions} from "./graphs.actions";
import {environment} from "../../../environments/environment";
/**
 * Created by bmorales on 5/14/17.
 */
@Injectable()
export class GraphsEffects {
  constructor(
    private http: Http,
    private actions$: Actions
  ) { }


  @Effect()
  addSeries$ = this.actions$
  // Listen for the 'ADD_SERIES' action
    .ofType(GraphsActions.ADD_SERIES)
    //use that payload to construct a data query
    .map(action => action.payload)
    .switchMap(payload => this.http.get(environment.apiURL + "/data/" + payload.series.site + "/" + payload.series.name + "/raw?startDate=" + payload.graph.startTime.toISOString() + "&endDate=" + payload.graph.endTime.toISOString())
    // If successful, dispatch success action with result
      .map(res => {
        return {
          type: GraphsActions.SERIES_DATA,
          payload: {
            graph: payload.graph,
            series: payload.series,
            data: res.json()
          }
        }
      })
      // If request fails, dispatch failed action
      .catch(() => Observable.of({ type: 'LOGIN_FAILED' })));
}
