import {Series} from "./series.model";
/**
 * A Graph, represented in the UI as a D3 graph more fields can probably be added later
 */
export interface Graph {
  name: string;
  startTime: Date;
  endTime: Date;
  id: string;
  series: { [id: string]: Series };
}
