
import {ChangeDetectionStrategy, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from "@angular/core";
import * as D3 from 'd3/index';
import {Graph} from "../../../common/graphs/graph.model";
@Component({
  selector: 'chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss']
})
export class ChartComponent implements OnChanges, OnInit{

  @Input() graph: Graph;
  @Output() hoveredSeries: EventEmitter<any> = new EventEmitter<any>();

  private initialized = false;

  private margin = {
    top: 20,
    bottom: 20,
    right: 20,
    left: 20
  };
  private width: number;
  private height: number;

  private x;

  constructor(private element: ElementRef) {

  }

  ngOnChanges(changes: SimpleChanges) {
    if(!this.initialized) return;
    //update the x axis
    this.updateXAxis();

    //now lets loop through all the series and update them
    for(let seriesName in this.graph.series) {
      if(this.graph.series.hasOwnProperty(seriesName)) {
        if(this.graph.series[seriesName].data !== undefined) {
          this.updateSeries(seriesName);
        }
      }
    }

  }

  private getSVG() {
    return D3.select(this.element.nativeElement.children[0]);
  }

  private getMainGroup() {
    return this.getSVG().select(".mainGroup");
  }


  ngOnInit() {
    //setup main chart svg element and bounds
    let svg = this.getSVG();
    this.width = this.element.nativeElement.children[0].getBoundingClientRect().width - this.margin.left - this.margin.right;
    this.height = this.element.nativeElement.children[0].getBoundingClientRect().height - this.margin.top - this.margin.bottom;
    let mainGroup = svg.append("g").attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")").attr("class", "mainGroup");

    this.updateXAxis();
    this.makeToolTip();
    this.initialized = true;
  }

  makeToolTip() {

    let x = this.x;
    let graph = this.graph;
    let hoveredSeries = this.hoveredSeries;

    this.getSVG().append("rect")
      .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")")
      .attr("class", "overlay")
      .attr("width", this.width)
      .attr("height", this.height)
      .attr("style", function() {return "fill:none;pointer-events:all"})
        //use these two if you want to indicate what you're hovering on
      //.on("mouseover", function() { focus.style("display", null); })
      //.on("mouseout", function() { focus.style("display", "none"); })
      .on("mousemove", function() {
        let x0 = x.invert(D3.mouse(this)[0]);
        let bisectDate = D3.bisector(function (d) {
          return new Date(d.date).getTime();
        }).left;
        let datum = [];
        for (let seriesName in graph.series) {
          if (graph.series.hasOwnProperty(seriesName)) {
            let series = graph.series[seriesName];
            if (series.data === undefined || series.data.length == 0) continue;

            let x0 = x.invert(D3.mouse(this)[0]);
            let i = bisectDate(series.data, x0, 1);
            let d0 = series.data[i - 1];
            let d1 = series.data[i];
            if(d0 === undefined  || d1 === undefined) {
              continue;
            }
            let d = x0 - d0.date > d1.date - x0 ? d1 : d0;
            d.color = series.color;
            datum.push(d);
          }
        }
        hoveredSeries.emit(datum);
      });
  }


  updateXAxis() {
    //draw a brand new x-axis
    if(this.x === undefined) {
      //the x-axis
      this.x = D3.scaleTime().rangeRound([0, this.width]);
      this.x.domain(D3.extent([this.graph.startTime, this.graph.endTime], function(d) { return d; }));

      //select the x-axis
      this.getMainGroup().append("g")
        .classed("x axis", true)
        .attr("transform", "translate(0," + this.height + ")")
        .call(D3.axisBottom(this.x));
    } else {
      //update the existing axis
      let t = D3.transition()
        .duration(750);
      this.x.domain(D3.extent([this.graph.startTime, this.graph.endTime], function(d) { return d; }));
      this.getMainGroup().select("g.x.axis")
        .transition(t)
        .call(D3.axisBottom(this.x));
    }
  }

  updateSeries(seriesName: string) {
    let series = this.graph.series[seriesName];
    let className = series.name.replace(/\./g, "_").replace("/ /g", "_");

    let y = D3.scaleLinear()
      .rangeRound([this.height, 0]);

    y.domain(D3.extent(series.data, function(d) { return d.value; }));
    let x = this.x;

    let line = D3.line()
      .x(function(d) {
        return x(new Date(d.date));
      })
      .y(function(d) {
        return y(d.value);
      });

    let t = D3.transition()
      .duration(750);

    //select and set data
    let path = this.getMainGroup().selectAll("." + className)
      .data([series.data], function(d) {
        return d;
      });

    path.transition(t).attr("d", line);

    path.enter().append("path").classed(className, true)
      .merge(path)
      .attr("fill", "none")
      .attr("stroke", series.color)
      .attr("stroke-linejoin", "round")
      .attr("stroke-linecap", "round")
      .attr("stroke-width", 1.5)
      .transition(t)
      .attr("d", line)

    //now the y-axis
    let yAxis = this.getMainGroup()
      .select("." + className);
    yAxis.enter()
      .append("g")
        .call(D3.axisLeft(y))
        .append("text")
          .attr("fill", "#000")
          .attr("transform", "rotate(-90)")
          .attr("y", 6)
          .attr("dy", "0.71em")
          .attr("text-anchor", "end")
          .text("Price ($)");


  }

}
