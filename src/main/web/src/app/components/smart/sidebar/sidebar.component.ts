
import {Graph} from "../../../common/graphs/graph.model";
import {Component, Input, OnInit} from "@angular/core";
import {GraphsActions} from "../../../common/graphs/graphs.actions";
import {AppState} from "../../../appstate.reducer";
import {Store} from "@ngrx/store";
import {Subject} from "rxjs";

@Component({
  selector: 'sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent{
  @Input() graph: Graph;

  public editName: false;
  private editNameChanged: Subject<string> = new Subject<string>();
  private seriesName = "";
  private siteName = "";
  public color="#ff0000";

  constructor(private store: Store<AppState>){
  }

  seriesNameChanged(event: any) {
    if(event !== null){
      this.seriesName = event.dlTagName;
      this.siteName = event.siteId;
    }
    else {
      this.seriesName = null;
      this.siteName = null;
    }

  }

  finalizeNameChange(event) {
    if(event.keyCode === 13 || (event.key !== undefined && event.key === "Enter")) {
      this.updateValue("name", event.target.value, GraphsActions.EDIT_GRAPH);
      this.editName = false;
    }
  }

  finalizeStartTime(event) {
    this.finalizeTime(event, "startTime");
  }

  finalizeEndTime(event) {
    this.finalizeTime(event, "endTime");
  }

  finalizeTime(event, field) {
    if (event.keyCode === undefined && (typeof event) === "string"){
      let d = new Date(event);
      let startingDate: Date = this.graph[field];
      let adjustedDate = new Date(d.getTime() + new Date().getTimezoneOffset() * 60 * 1000 + startingDate.getHours() * 60 * 60 * 1000 + startingDate.getMinutes() * 60 * 1000 + startingDate.getSeconds() * 1000);
      this.updateValue(field, adjustedDate, GraphsActions.CHANGE_DATES);
    }
    else if(event.keyCode === 13 || (event.key !== undefined && event.key === "Enter")) {
      let d = new Date(event.target.value);
      let startingDate: Date = this.graph[field];
      let adjustedDate = new Date(d.getTime() + new Date().getTimezoneOffset() * 60 * 1000 + startingDate.getHours() * 60 * 60 * 1000 + startingDate.getMinutes() * 60 * 1000 + startingDate.getSeconds() * 1000);
      this.updateValue(field, adjustedDate, GraphsActions.CHANGE_DATES);
    }
  }

  updateValue(name: string, value: any, action: string) {
    let editGraph = Object.assign({}, this.graph);
    editGraph[name] = value;
    this.store.dispatch({type: action, payload: editGraph});
  }

  canAdd() {
    return this.seriesName !== null && this.siteName !== null;
  }

  addData() {
    let editGraph = Object.assign({}, this.graph);
    let newSeries = {
      name: this.seriesName,
      site: this.siteName,
      color: this.color
    };
    this.store.dispatch({type: GraphsActions.ADD_SERIES, payload: {
      graph: editGraph,
      series: newSeries
    }});
    this.siteName = "";
    this.seriesName = "";
  }


  get name() {
    return this.graph.name;
  }

  get series() {
    let val = []
    for(let property in this.graph.series) {
      if(this.graph.series.hasOwnProperty(property)) {
        val.push(this.graph.series[property]);
      }
    }
    return val;
  }


}
