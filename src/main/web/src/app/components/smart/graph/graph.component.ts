
import {Component, Input, OnInit} from "@angular/core";
import {Graph} from "../../../common/graphs/graph.model";

@Component({
  selector: 'graph',
  templateUrl: './graph.component.html',
  styleUrls: ['./graph.component.scss']
})
export class GraphComponent implements OnInit{

  @Input() graph: Graph;

  public hoveredSeries = [];
  constructor() {

  }

  ngOnInit() {
    console.log("GRAPH INIT")
  }

  get name() {
    return this.graph.name;
  }

  get series() {
    let val = [];
    for(let property in this.graph.series) {
      if(this.graph.series.hasOwnProperty(property)) {
        val.push(this.graph.series[property]);
      }
    }
    return val;
  }

  get startTime() {
    return this.graph.startTime;
  }

  get endTime() {
    return this.graph.endTime;
  }

  seriesHovered(event) {
    this.hoveredSeries = event;
  }

  download() {
    var dataArray = [];
    //make header
    let makeHeader = function(data) {
      let row = [];
      for(let property in data) {
        if(data.hasOwnProperty(property)) {
          row.push(property);
        }
      }
      dataArray.push(row);
    };

    for(let seriesName in this.graph.series) {
      if(this.graph.series.hasOwnProperty(seriesName)) {
        let series = this.graph.series[seriesName];
        if(series.data === undefined || series.data.length === 0) continue;
        if(dataArray.length === 0) makeHeader(series.data[0]);

        series.data.forEach(dat => {
          let row = [];
          for(let property in dat) {
            if(dat.hasOwnProperty(property)) {
              if(dat[property] === null || dat[property] === undefined) {
                row.push("");
              }else {
                row.push(dat[property].toString());
              }
            }
          }
          dataArray.push(row);
        })
      }
    }
    let csvContent = "data:text/csv;charset=utf-8,";
    dataArray.forEach(function(infoArray, index){
      let dataString = infoArray.join(",");
      csvContent += index < dataArray.length ? dataString+ "\n" : dataString;

    });
    let encodedUri = encodeURI(csvContent);
    window.open(encodedUri);
  }
}
