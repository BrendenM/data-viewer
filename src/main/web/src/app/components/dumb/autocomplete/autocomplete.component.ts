import {Component, EventEmitter, Input, OnInit, Output} from "@angular/core";
import {Subject} from "rxjs";
import {Http} from "@angular/http";
import {environment} from "../../../../environments/environment";
@Component({
  selector: 'autocomplete',
  templateUrl: './autocomplete.component.html',
  styleUrls: ['./autocomplete.component.scss']
})
export class AutocompleteComponent implements OnInit{


  @Input() searchTemplate: string;
  @Output() selectedValue: EventEmitter<any> = new EventEmitter<any>();

  private valueChangedSubject: Subject<string> = new Subject<string>();
  public value: string = "";
  private valueValid: boolean = false;

  private removeOptionsSubject: Subject<boolean> = new Subject<boolean>();
  public options = [];
  private highlitedOption = "";

  constructor(private http: Http) {

  }

  ngOnInit() {
    this.valueChangedSubject.debounceTime(300).subscribe(value => {
      this.performSearch();
    });
    this.removeOptionsSubject.debounceTime(300).subscribe(value => {
      if(value) {
        this.removeOptions();
      }
    });
  }

  public valueChanged(event) {
    this.selectedValue.emit(null);
    this.valueValid = false;
    this.options = [];
    this.highlitedOption = "";
    this.valueChangedSubject.next(event);
  }

  private performSearch() {
    let url = this.searchTemplate + this.value + "?page=0&size=10";
    this.http.get(environment.apiURL + url).subscribe(success => {
      let response = success.json();
      if(response.content !== null) {
        this.options = response.content.map(dat => {
          return {
            dlTagName: dat.dlTagName,
            siteId: dat.siteId
          }
        });
      }
    },
    error => {
      console.log(error);
    })
  }

  optionHighlite(option) {
    this.highlitedOption = option;
  }
  optionClicked(option) {
    this.selectedValue.emit(option);
    this.value = option.dlTagName;
    this.valueValid = true;
    this.removeOptionsSubject.next(false);
    this.options = [];
    this.highlitedOption = "";
  }
  getOptionColor(option) {
    if(option === this.highlitedOption) {
      return "lightblue";
    }else {
      return "";
    }
  }

  backgroundColorValid() {
    if(this.valueValid) {
      return "lightgreen";
    } else {
      return "white";
    }
  }

  blur(event) {
    this.removeOptionsSubject.next(true);
  }

  public removeOptions() {
    this.valueValid = false;
    this.selectedValue.emit(null);
    this.value = "";
    this.options = [];
    this.highlitedOption = "";
  }

}
