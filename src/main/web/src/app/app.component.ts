import { Component } from '@angular/core';
import {AppState, getGraphIds, getAllGraphs, getGraphsMap, getSelectedGraph} from "./appstate.reducer";
import {combineReducers, Store} from "@ngrx/store";
import {GraphsActions} from "./common/graphs/graphs.actions";
import {Observable} from "rxjs";
import {Graph} from "./common/graphs/graph.model";
import {createSelector} from "reselect";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  public graphs$: Observable<Graph[]>;
  private ids$: Observable<string[]>;
  public selectedGraph$: Observable<Graph>;

  constructor(private store: Store<AppState>){
    this.graphs$ = store.select(getAllGraphs);
    this.ids$ = store.select(getGraphIds);
    this.selectedGraph$ = store.select(getSelectedGraph);
  }

  click() {
    this.store.dispatch({type: GraphsActions.ADD_GRAPH});
  }

  selectGraph(graph: Graph) {
    this.store.dispatch({type: GraphsActions.SELECT_GRAPH, payload: graph.id});
  }

  trackGraph(index, graph: Graph) {
    return graph ? graph.id : undefined;
  }
}
