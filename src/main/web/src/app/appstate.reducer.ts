/*
 This metareducer wraps the reducers for the individual datastores and is imported in app.module
 */

import { compose } from '@ngrx/core/compose';
import { ActionReducer, combineReducers } from '@ngrx/store';

import * as fromGraphs from './common/graphs/graphs.reducer';
import {createSelector} from "reselect";

export interface AppState {
  graphs: fromGraphs.GraphsState
}

export const reducers = {
  graphs: fromGraphs.graphsReducer
};

// Generate a reducer to set the root state in dev mode for HMR
function stateSetter(reducer: ActionReducer<any>): ActionReducer<any> {
  return function (state, action) {
    if (action.type === 'SET_ROOT_STATE') {
      return action.payload;
    }
    return reducer(state, action);
  };
}


const DEV_REDUCERS = [stateSetter];

const developmentReducer = compose(...DEV_REDUCERS, combineReducers)(reducers);
// const productionReducer = compose(resetOnLogout, combineReducers)(reducers);

export function rootReducer(state: any, action: any) {
  // if (ENV !== 'development') {
  //   return productionReducer(state, action);
  // } else {
    return developmentReducer(state, action);
  // }
}

export const getGraphsState = (state: AppState) => state.graphs;
export const getGraphIds = createSelector(getGraphsState, fromGraphs.getIds);
export const getGraphsMap = createSelector(getGraphsState, fromGraphs.getGraphsMap);
export const getAllGraphs = createSelector(getGraphsState, fromGraphs.getAll);
export const getSelectedGraph = createSelector(getGraphsState, fromGraphs.getSelected);
