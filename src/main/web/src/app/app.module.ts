import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { HttpModule } from '@angular/http';

import {StoreModule} from "@ngrx/store";
import { rootReducer as reducer } from './appstate.reducer';
import {CommonModule} from "@angular/common";

import { AppComponent } from './app.component';
import {GraphComponent} from "./components/smart/graph/graph.component";
import {ChartComponent} from "./components/smart/chart/chart.component";
import {SidebarComponent} from "./components/smart/sidebar/sidebar.component";
import {AutocompleteComponent} from "./components/dumb/autocomplete/autocomplete.component";
import {EffectsModule} from "@ngrx/effects";
import {GraphsEffects} from "./common/graphs/graphs.effects";

@NgModule({
  declarations: [
    AppComponent,
    GraphComponent,
    ChartComponent,
    SidebarComponent,
    AutocompleteComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    EffectsModule.run(GraphsEffects),

    /**
     * StoreModule.provideStore is imported once in the root module, accepting a reducer
     * function or object map of reducer functions. If passed an object of
     * reducers, combineReducers will be run creating your application
     * meta-reducer. This returns all providers for an @ngrx/store
     * based application.
     */
    StoreModule.provideStore(reducer),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
